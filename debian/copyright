Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: UniFrac Binaries
Source: https://github.com/biocore/unifrac-binaries
Comment:
 The original source contains some CC-BY-SA snippets by Stack Overflow users
 (https://stackoverflow.com/questions/8401777/simple-glob-in-c-on-unix-system,
 https://stackoverflow.com/a/217605,
 https://stackoverflow.com/a/19841704/19741,
 https://stackoverflow.com/a/11583550/19741,
 https://stackoverflow.com/a/23587285/19741,
 https://stackoverflow.com/a/14539953, https://stackoverflow.com/a/868894,
 https://stackoverflow.com/a/33308902/379593) but these may be too small to be
 copyrightable, and the less trivial ones are mentioned.

Files: *
Copyright: 2022 biocore
License: BSD-3-Clause

Files: src/biom.cpp
       src/biom.hpp
       src/biom_interface.hpp
       src/tree.hpp
       src/unifrac.cpp
       src/unifrac.hpp
       src/unifrac_cmp.cpp
       src/unifrac_cmp.hpp
       src/unifrac_internal.cpp
       src/unifrac_internal.hpp
       src/unifrac_task.hpp
Copyright: 2016-2021 UniFrac development team.
License: BSD-3-Clause
Comment:
 The corresponding code migrated from the existing project unifrac[1] following
 a split of its source code in two.
 .
 [1]: https://github.com/biocore/unifrac

Files: debian/*
Copyright: 2022 Andreas Tille <tille@debian.org>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
