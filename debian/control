Source: unifrac-tools
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               libhdf5-dev,
               hdf5-helpers,
               liblz4-dev,
               libblas-dev,
               liblapacke-dev,
               chrpath,
               gfortran,
               python3 <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/unifrac-tools
Vcs-Git: https://salsa.debian.org/med-team/unifrac-tools.git
Homepage: https://github.com/biocore/unifrac-binaries
Rules-Requires-Root: no

Package: unifrac-tools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libssu0
Conflicts: libssu-tools
Provides: libssu-tools
Replaces: libssu-tools
Description: high-performance phylogenetic diversity calculations (binaries)
 The de facto repository for high-performance phylogenetic diversity
 calculations. The methods in this repository are based on an
 implementation of the Strided State UniFrac algorithm which is faster,
 and uses less memory than Fast UniFrac. Strided State UniFrac supports
 Unweighted UniFrac, Weighted UniFrac, Generalized UniFrac, Variance
 Adjusted UniFrac and meta UniFrac. This repository also includes Stacked
 Faith (manuscript in preparation), a method for calculating Faith's PD
 that is faster and uses less memory than the Fast UniFrac-based
 reference implementation.
 .
 This package contains target binaries ssu and faithpd

Package: libssu0
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: high-performance phylogenetic diversity calculations (lib)
 The de facto repository for high-performance phylogenetic diversity
 calculations. The methods in this repository are based on an
 implementation of the Strided State UniFrac algorithm which is faster,
 and uses less memory than Fast UniFrac. Strided State UniFrac supports
 Unweighted UniFrac, Weighted UniFrac, Generalized UniFrac, Variance
 Adjusted UniFrac and meta UniFrac. This repository also includes Stacked
 Faith (manuscript in preparation), a method for calculating Faith's PD
 that is faster and uses less memory than the Fast UniFrac-based
 reference implementation.
 .
 This package contains the shared library.

Package: libssu-dev
Architecture: any
Section: libdevel
Depends: libssu0 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: high-performance phylogenetic diversity calculations (dev)
 The de facto repository for high-performance phylogenetic diversity
 calculations. The methods in this repository are based on an
 implementation of the Strided State UniFrac algorithm which is faster,
 and uses less memory than Fast UniFrac. Strided State UniFrac supports
 Unweighted UniFrac, Weighted UniFrac, Generalized UniFrac, Variance
 Adjusted UniFrac and meta UniFrac. This repository also includes Stacked
 Faith (manuscript in preparation), a method for calculating Faith's PD
 that is faster and uses less memory than the Fast UniFrac-based
 reference implementation.
 .
 This package contains the static library and header files.
